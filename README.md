# bepermunicipal

Projeção de pessoas a receber benefício emergencial suplementar de municípios que tomem a importante iniciativa de fazê-lo.

Busca-se basear a projeção em quatro(?) fontes:

1) CadÚnico <- há informação bastante detalhada de pessoas e famílias que deveriam receber ajuda suplementar

2) CAGED - informação mais completa sobre o mercado formal

3) PNAD contínua - informação com grau importante de detalhamento, universo maior de pessoas do que o CadÚnico e CAGED, ainda que seja amostral. Para cidades grandes em regiões metropolitanas, permite desagregação por bairro

4) DATASUS - informação detalhada e muito atualizada de morbidades <- procurar morbidades correlacionadas com maior desemprego/dificuldades de renda?





